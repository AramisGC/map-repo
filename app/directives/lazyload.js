mapApp.directive('lazyLoad', function() { // the lazy load directive to load only the images that are visible in the viewport
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      const observer = new IntersectionObserver(loadImg);
      const img = angular.element(element)[0];
      observer.observe(img);

      function loadImg(changes) {
        changes.forEach(change => {
          if (change.intersectionRatio > 0) {
            change.target.src = change.target.attributes['tile-src'].value; // if image is visible in viewport, then load the tile image from tile-src attribute
          }
        });
      }    
    }
  }
});