mapApp.filter('range', function() { // this filter calculates POW of two dimension size
  return function(input, total) {
    total = parseInt(total);
    total = Math.pow(2, total - 1);

    for (var i = 0; i < total; i++) {
      input.push(i);
    }
    return input;
  };
});