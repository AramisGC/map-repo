mapApp.controller('mapController', ['$scope', function($scope) { // main map app controller

  // app states
  $scope.state = {
    zoom: 4,
    maxZoom: 4,
    mouseEnterX: 0, mouseEnterY: 0,
    paneX: 0, paneY: 0,
    mouseDown: false,
  };

  $scope.zoomIn = function() {
    if ($scope.state.zoom < $scope.state.maxZoom) {
      $scope.state.zoom++;
    }
  };

  $scope.zoomOut = function() {
    if ($scope.state.zoom > 1) {
      $scope.state.zoom--;
    }
  };

  $scope.mouseDown = function(e) {
    $scope.state.mouseDown = true;
    $scope.state.mouseEnterX = e.pageX - $scope.state.paneX;
    $scope.state.mouseEnterY = e.pageY - $scope.state.paneY;
    e.preventDefault();
  };

  $scope.mouseUp = function(e) {
    $scope.state.mouseDown = false;
    e.preventDefault();
  };

  $scope.mouseMove = function(e) {
    if ($scope.state.mouseDown) {
      if (e.pageX || e.pageY) {
        $scope.state.paneX = e.pageX - $scope.state.mouseEnterX;
        $scope.state.paneY = e.pageY - $scope.state.mouseEnterY;
      }
    }
    e.preventDefault();
  };

  $scope.mouseLeave = function(e) {
    $scope.state.mouseDown = false;
    e.preventDefault();
  };

}]);